# EJERCICIO 13.6. VÍDEOS EN CANAL DE YOUTUBE (CON DESCARGA)


from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
from urllib.request import urlopen

videos = ""


class YTHandler(ContentHandler):
    def __init__(self):
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.title = ""
        self.link = ""

    def startElement(self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')

    def endElement(self, name):
        global videos

        if name == 'entry':
            self.inEntry = False
            videos = videos + "<li><a href='" + self.link + "'>" + self.title + "</a></li>\n"
        elif self.inEntry:
            if name == 'title':
                self.title = self.content
                self.content = ""
                self.inContent = False

    def characters(self, chars):
        if self.inContent:
            self.content = self.content + chars


Parser = make_parser()
Parser.setContentHandler(YTHandler())


if __name__ == "__main__":

    PAGE = """
    <!DOCTYPE html>
    <html lang="en">
      <body>
        <h1>Channel contents:</h1>
        <ul>
    {videos}
        </ul>
      </body>
    </html>
    """

if len(sys.argv) < 2:
    print("Usage: python xml-parser-youtube.py <id>")
    print()
    print(" <id>: id of the Youtube channel to parse")
    sys.exit(1)


url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' + sys.argv[1]
xmlStream = urlopen(url)
Parser.parse(xmlStream)
page = PAGE.format(videos=videos)
print(page)

